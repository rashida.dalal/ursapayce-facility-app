//
//  AppDelegate.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 9/01/24.
//

import UIKit
import FirebaseCore

var userDefl = UserDefaults.standard


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    var orientationLock = UIInterfaceOrientationMask.landscape
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        let notice = SplashViewController.getReferenceFromStoryBoard("Main")
        let navigationController = UINavigationController()
        navigationController.viewControllers = [notice]

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        
        return true
    }

    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }


}


struct Orientation {
    // indicate current device is in the LandScape orientation
    static var isLandscape: Bool {
        get {
            return UIDevice.current.orientation.isValidInterfaceOrientation
                ? UIDevice.current.orientation.isLandscape
                : (UIApplication.shared.windows.first?.windowScene?.interfaceOrientation.isLandscape)!
        }
    }
}
