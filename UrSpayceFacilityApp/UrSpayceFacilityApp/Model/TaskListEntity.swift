//
//  TaskListEntity.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 12/01/24.
//

import Foundation
import ObjectMapper

struct TaskListEntity : Mappable {
    var count : Int?
    var data : [TaskListData]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        count <- map["count"]
        data <- map["data"]
    }

}

struct TaskListData : Mappable {
    var _id : String?
    var title : String?
    var frequency : String?
    var section : Section?
    var floor : String?
    var brand : String?
    var venue : String?
    var creator : Creator?
    var status : String?
    var isRecurring : Bool?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var assignedTo : AssignedTo?
    var select = false
    var showSelectView = false

    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        title <- map["title"]
        frequency <- map["frequency"]
        section <- map["section"]
        floor <- map["floor"]
        brand <- map["brand"]
        venue <- map["venue"]
        creator <- map["creator"]
        status <- map["status"]
        isRecurring <- map["isRecurring"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        assignedTo <- map["assignedTo"]
    }

}

struct Creator : Mappable {
    var _id : String?
    var name : String?
    var email : String?
    var phone : Phone?
    var phoneVerified : Bool?
    var hash : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var venue : String?
    var device : Device?
    var timeZone : String?
    var gender : String?
    var secret : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        phoneVerified <- map["phoneVerified"]
        hash <- map["hash"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        venue <- map["venue"]
        device <- map["device"]
        timeZone <- map["timeZone"]
        gender <- map["gender"]
        secret <- map["secret"]
    }

}

struct AssignedTo : Mappable {

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

    }

}

struct Section : Mappable {
    var _id : String?
    var name : String?
    var startDate : String?
    var endDate : String?
    var brand : String?
    var creator : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var app : String?
    var manager : String?
    var superwiser : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        brand <- map["brand"]
        creator <- map["creator"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        app <- map["app"]
        manager <- map["manager"]
        superwiser <- map["superwiser"]
    }

}

struct Device : Mappable {
    var userAgent : String?
    var os : String?
    var browser : String?
    var device : String?
    var os_version : String?
    var browser_version : String?
    var deviceType : String?
    var orientation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        userAgent <- map["userAgent"]
        os <- map["os"]
        browser <- map["browser"]
        device <- map["device"]
        os_version <- map["os_version"]
        browser_version <- map["browser_version"]
        deviceType <- map["deviceType"]
        orientation <- map["orientation"]
    }

}
