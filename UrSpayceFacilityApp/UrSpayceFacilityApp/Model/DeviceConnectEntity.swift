//
//  DeviceConnectEntity.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import Foundation
import ObjectMapper

struct DeviceConnectEntity : Mappable {
    
    var status : Bool?
    var message : String?
    var verified : Bool?
    var brand : Brand?
    var venue : Venue?
    var authToken : String?
    var creator : String?
    var device : DeviceConnect?
    
    

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        verified <- map["verified"]
        brand <- map["brand"]
        venue <- map["venue"]
        authToken <- map["authToken"]
        creator <- map["creator"]
        device <- map["device"]
    }

}

struct Brand : Mappable {
    var _id : String?
    var name : String?
    var website : String?
    var description : String?
    var email : String?
    var phone : Phone?
    var partner : String?
    var objectType : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var active : Bool?
    var companyType : String?
    var location : String?
    var formattedAddress : String?
    var image : Image?
    var apps : [Apps]?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        website <- map["website"]
        description <- map["description"]
        email <- map["email"]
        phone <- map["phone"]
        partner <- map["partner"]
        objectType <- map["objectType"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        active <- map["active"]
        companyType <- map["companyType"]
        location <- map["location"]
        formattedAddress <- map["formattedAddress"]
        image <- map["image"]
        apps <- map["apps"]
    }

}

struct DeviceConnect : Mappable {
    
    var _id : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
    }

}

struct Venue : Mappable {
    var _id : String?
    var name : String?
    var location : Location?
    var brand : Brand?
    var active : Bool?
    var siteType : SiteType?
    var venueType : VenueType?
    var geometry : Geometry?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var reviewsVisibility : Bool?
    var setupProgress : Int?
    var building : Building?
    var buildingType : String?
    var images : [Images]?
    var inventory : [Inventory]?
    var amenities : [String]?
    var pricing : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        location <- map["location"]
        brand <- map["brand"]
        active <- map["active"]
        siteType <- map["siteType"]
        venueType <- map["venueType"]
        geometry <- map["geometry"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        reviewsVisibility <- map["reviewsVisibility"]
        setupProgress <- map["setupProgress"]
        building <- map["building"]
        buildingType <- map["buildingType"]
        images <- map["images"]
        inventory <- map["inventory"]
        amenities <- map["amenities"]
        pricing <- map["pricing"]
    }

}

struct Phone : Mappable {
    var number : String?
    var internationalNumber : String?
    var nationalNumber : String?
    var e164Number : String?
    var countryCode : String?
    var dialCode : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        number <- map["number"]
        internationalNumber <- map["internationalNumber"]
        nationalNumber <- map["nationalNumber"]
        e164Number <- map["e164Number"]
        countryCode <- map["countryCode"]
        dialCode <- map["dialCode"]
    }

}

struct Image : Mappable {
    var _id : String?
    var objectId : String?
    var objectType : String?
    var coverImage : Bool?
    var name : String?
    var active : Bool?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        objectId <- map["objectId"]
        objectType <- map["objectType"]
        coverImage <- map["coverImage"]
        name <- map["name"]
        active <- map["active"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
    }

}

struct Apps : Mappable {
    var _id : String?
    var app : String?
    var brand : String?
    var active : Bool?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var creator : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        app <- map["app"]
        brand <- map["brand"]
        active <- map["active"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        creator <- map["creator"]
    }

}

struct SiteType : Mappable {
    var _id : String?
    var name : String?
    var brand : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var description : String?
    var index : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        brand <- map["brand"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        description <- map["description"]
        index <- map["index"]
    }

}


struct VenueType : Mappable {
    var _id : String?
    var name : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var siteType : String?
    var description : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        siteType <- map["siteType"]
        description <- map["description"]
    }

}

struct Building : Mappable {
    var _id : String?
    var buildingType : String?
    var brand : String?
    var name : String?
    var address : String?
    var ownerArray : [OwnerArray]?
    var constructionYear : String?
    var commonEntry : Bool?
    var commonFacing : String?
    var entry : Int?
    var entryFacing : String?
    var exits : Int?
    var exitFacing : String?
    var floor : Int?
    var basement : Int?
    var fireExit : Bool?
    var fireExitCount : Int?
    var lifts : Bool?
    var liftCount : Int?
    var rera : String?
    var area : Int?
    var height : Int?
    var cam : Int?
    var parkingRatio : String?
    var airConditioning : String?
    var ownershipStructure : String?
    var location : Location?
    var creator : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var visible : Bool?
    var dgBackup : String?
    var ground : Int?
    var powerBackup : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        buildingType <- map["buildingType"]
        brand <- map["brand"]
        name <- map["name"]
        address <- map["address"]
        ownerArray <- map["ownerArray"]
        constructionYear <- map["constructionYear"]
        commonEntry <- map["commonEntry"]
        commonFacing <- map["commonFacing"]
        entry <- map["entry"]
        entryFacing <- map["entryFacing"]
        exits <- map["exits"]
        exitFacing <- map["exitFacing"]
        floor <- map["floor"]
        basement <- map["basement"]
        fireExit <- map["fireExit"]
        fireExitCount <- map["fireExitCount"]
        lifts <- map["lifts"]
        liftCount <- map["liftCount"]
        rera <- map["rera"]
        area <- map["area"]
        height <- map["height"]
        cam <- map["cam"]
        parkingRatio <- map["parkingRatio"]
        airConditioning <- map["airConditioning"]
        ownershipStructure <- map["ownershipStructure"]
        location <- map["location"]
        creator <- map["creator"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        visible <- map["visible"]
        dgBackup <- map["dgBackup"]
        ground <- map["ground"]
        powerBackup <- map["powerBackup"]
    }

}

struct OwnerArray : Mappable {
    var type : String?
    var company : String?
    var name : String?
    var email : String?
    var phone : Phone?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        type <- map["type"]
        company <- map["company"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
    }

}

struct Inventory : Mappable {
    var _id : String?
    var name : String?
    var floor : String?
    var inventoryType : String?
    var capacity : Int?
    var description : String?
    var venue : String?
    var unit : String?
    var location : String?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var occupancy : String?
    var occupancyDate : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        floor <- map["floor"]
        inventoryType <- map["inventoryType"]
        capacity <- map["capacity"]
        description <- map["description"]
        venue <- map["venue"]
        unit <- map["unit"]
        location <- map["location"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        occupancy <- map["occupancy"]
        occupancyDate <- map["occupancyDate"]
    }

}

struct Location : Mappable {
    var _id : String?
    var name : String?
    var formattedAddress : String?
    var subLocality2 : String?
    var subLocality1 : String?
    var locality : String?
    var city : String?
    var state : String?
    var country : String?
    var zip : String?
    var latitude : Double?
    var longitude : Double?
    var openingHours : [String]?
    var business_status : String?
    var rating : Double?
    var reviews : [Reviews]?
    var url : String?
    var userRatingTotal : Int?
    var photoes : [Photoes]?
    var googlePlaceId : String?
    var plus_code : Plus_code?
    var types : [String]?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?
    var nearBy : [NearBy]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        name <- map["name"]
        formattedAddress <- map["formattedAddress"]
        subLocality2 <- map["subLocality2"]
        subLocality1 <- map["subLocality1"]
        locality <- map["locality"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        zip <- map["zip"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        openingHours <- map["openingHours"]
        business_status <- map["business_status"]
        rating <- map["rating"]
        reviews <- map["reviews"]
        url <- map["url"]
        userRatingTotal <- map["userRatingTotal"]
        photoes <- map["photoes"]
        googlePlaceId <- map["googlePlaceId"]
        plus_code <- map["plus_code"]
        types <- map["types"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
        nearBy <- map["nearBy"]
    }

}

struct Images : Mappable {
    var _id : String?
    var objectId : String?
    var objectType : String?
    var coverImage : Bool?
    var name : String?
    var active : Bool?
    var creationDate : String?
    var modificationDate : String?
    var markForDelete : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        objectId <- map["objectId"]
        objectType <- map["objectType"]
        coverImage <- map["coverImage"]
        name <- map["name"]
        active <- map["active"]
        creationDate <- map["creationDate"]
        modificationDate <- map["modificationDate"]
        markForDelete <- map["markForDelete"]
    }

}

struct Geometry : Mappable {
    var location : Location?
    var viewport : Viewport?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        location <- map["location"]
        viewport <- map["viewport"]
    }

}

struct Viewport : Mappable {
    var northeast : DirectionNS?
    var southwest : DirectionNS?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        northeast <- map["northeast"]
        southwest <- map["southwest"]
    }

}

struct DirectionNS : Mappable {
    var lat : Double?
    var lng : Double?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        lat <- map["lat"]
        lng <- map["lng"]
    }

}

struct Reviews : Mappable {
    var author_name : String?
    var author_url : String?
    var language : String?
    var profile_photo_url : String?
    var rating : Int?
    var relative_time_description : String?
    var text : String?
    var time : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        author_name <- map["author_name"]
        author_url <- map["author_url"]
        language <- map["language"]
        profile_photo_url <- map["profile_photo_url"]
        rating <- map["rating"]
        relative_time_description <- map["relative_time_description"]
        text <- map["text"]
        time <- map["time"]
    }

}

struct Photoes : Mappable {
    var height : Int?
    var html_attributions : [String]?
    var width : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        height <- map["height"]
        html_attributions <- map["html_attributions"]
        width <- map["width"]
    }

}

struct Plus_code : Mappable {
    var compound_code : String?
    var global_code : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        compound_code <- map["compound_code"]
        global_code <- map["global_code"]
    }

}

struct NearBy : Mappable {
    var business_status : String?
    var geometry : Geometry?
    var icon : String?
    var name : String?
    var opening_hours : Opening_hours?
    var photos : [Photos]?
    var place_id : String?
    var plus_code : Plus_code?
    var rating : Int?
    var reference : String?
    var scope : String?
    var types : [String]?
    var user_ratings_total : Int?
    var vicinity : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        business_status <- map["business_status"]
        geometry <- map["geometry"]
        icon <- map["icon"]
        name <- map["name"]
        opening_hours <- map["opening_hours"]
        photos <- map["photos"]
        place_id <- map["place_id"]
        plus_code <- map["plus_code"]
        rating <- map["rating"]
        reference <- map["reference"]
        scope <- map["scope"]
        types <- map["types"]
        user_ratings_total <- map["user_ratings_total"]
        vicinity <- map["vicinity"]
    }

}

struct Opening_hours : Mappable {
    var open_now : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        open_now <- map["open_now"]
    }

}

struct Photos : Mappable {
    var height : Int?
    var html_attributions : [String]?
    var photo_reference : String?
    var width : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        height <- map["height"]
        html_attributions <- map["html_attributions"]
        photo_reference <- map["photo_reference"]
        width <- map["width"]
    }

}
