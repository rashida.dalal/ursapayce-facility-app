//
//  UIColorExtension.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import Foundation

import UIKit

// MARK: - App colors from Colors.xcassets

extension UIColor {
    static let backgroundColor = UIColor.fromAsset(named: "backgroundColor")
    static let bgBlueLightColor = UIColor.fromAsset(named: "bgBlueLightColor")
    static let borderColor = UIColor.fromAsset(named: "borderColor")
    static let BlueColor = UIColor.fromAsset(named: "BlueColor")
    static let buttonGrayColor = UIColor.fromAsset(named: "buttonGrayColor")
    static let eventGoldColor = UIColor.fromAsset(named: "eventGoldColor")
    static let eventPinkColor = UIColor.fromAsset(named: "eventPinkColor")
    static let gray = UIColor.fromAsset(named: "gray")
    static let DarkGray = UIColor.fromAsset(named: "DarkGray")
    static let LighterGray = UIColor.fromAsset(named: "LighterGray")
    static let lightGray = UIColor.fromAsset(named: "lightGray")
    static let LightBlueColor = UIColor.fromAsset(named: "LightBlueColor")
    static let RedColor = UIColor.fromAsset(named: "RedColor")
    static let searchBgColor = UIColor.fromAsset(named: "searchBgColor")
    static let shadowColor = UIColor.fromAsset(named: "shadowColor")
    
    
    
    
    
}


fileprivate extension UIColor {
    static func fromAsset(named name: String) -> UIColor {
        guard let color = UIColor(named: name) else {
            fatalError("Color \(name) is not defined in a color asset.")
        }
        return color
    }
}
