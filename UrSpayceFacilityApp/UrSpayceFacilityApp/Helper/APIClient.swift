//
//  APIClient.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import Foundation
import Alamofire
import ObjectMapper

class APIClient {
    
    static let window = (UIApplication.shared.delegate as! AppDelegate).window
    
    static func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: { action in
        }))
        vc.present(alert, animated: true, completion: nil)
        
        
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                //Globalfunc.print(object:error.localizedDescription)
            }
        }
        return nil
    }
    

    
    
    static func getTokenToConnect(param: [String:Any],completion: @escaping ([String: Any], Int?)->Void){
        
        let loginUrl = Constants.generate_token
        AF.request(loginUrl, method:.post, parameters: param,encoding: JSONEncoding.default) .responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                
                if let castingValue = val as? [String: Any] {
                    completion(castingValue,200)
                }
            case .failure(let error):
                let json = [String:Any]()
                completion(json,code)
            }
        }
    }
    
    
    static func verifyToken(param: [String:Any],completion: @escaping (DeviceConnectEntity, Int?)->Void){
        
        let loginUrl = Constants.verify_token
        AF.request(loginUrl, method:.post, parameters: param,encoding: JSONEncoding.default) .responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                
                if let castingValue = val as? [String: Any] {
                    guard let resp = Mapper<DeviceConnectEntity>().map(JSON: castingValue) else { return }
                    completion(resp,code)
                }
            case .failure(let error):
                let json = [String:Any]()
                guard let resp = Mapper<DeviceConnectEntity>().map(JSON: json) else {return}
                completion(resp,code)
            }
        }
    }
    
    
    static func getTaskList(param: [String:Any],completion: @escaping (TaskListEntity, Int?)->Void){
        
        let request = AF.request(Constants.task_schedule, method: .post, parameters: param, encoding: JSONEncoding.default, headers: APIManager.headers(), interceptor: nil)

        request.responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                
                if let castingValue = val as? [String: Any] {
                    print(castingValue)
                    guard let resp = Mapper<TaskListEntity>().map(JSON: castingValue) else { return }
                    completion(resp,code)
                }
            case .failure(let error):
                let json = [String:Any]()
                guard let resp = Mapper<TaskListEntity>().map(JSON: json) else {return}
                completion(resp,code)
            }
        }
    }
    
    static func getCheckCount(param: [String:Any],completion: @escaping (Any, Int?)->Void){
        
        let request = AF.request(Constants.checkCount, method: .post, parameters: param, encoding: JSONEncoding.default, headers: APIManager.headers(), interceptor: nil)

        request.responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                completion(val,code)
            case .failure(let error):
                let json = "error"
                completion(json,code)
            }
        }
    }
    
    
    static func updateTaskList(param: [String:Any],completion: @escaping (TaskListEntity, Int?)->Void){
        
        let request = AF.request(Constants.task_schedule, method: .put, parameters: param, encoding: JSONEncoding.default, headers: APIManager.headers(), interceptor: nil)

        request.responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                
                if let castingValue = val as? [String: Any] {
                    print(castingValue)
                    guard let resp = Mapper<TaskListEntity>().map(JSON: castingValue) else { return }
                    completion(resp,code)
                }
            case .failure(let error):
                let json = [String:Any]()
                guard let resp = Mapper<TaskListEntity>().map(JSON: json) else {return}
                completion(resp,code)
            }
        }
    }
    
    static func deleteDevice(delUrl: String, completion: @escaping (Any, Int?)->Void) {
        
        let request = AF.request(delUrl, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: APIManager.headers(), interceptor: nil)

        request.responseJSON { response in
            
            let statcode = response.response?.statusCode ?? 0
            print(statcode)
            switch response.result {
            case .success(let val):
                completion(val,statcode)
            case .failure(let error):
                completion(error,statcode)
            }
        }
    }
    
    
    static func SubmitReview(taskId:String, param: [String:Any],completion: @escaping ([String: Any], Int?)->Void){
        
        let url = "\(Constants.add_review)/\(taskId)"
        let request = AF.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: APIManager.headers(), interceptor: nil)

        request.responseJSON { response in
            
            let code = response.response?.statusCode ?? 0
            
            switch response.result {

            case .success(let val):
                if let castingValue = val as? [String: Any] {
                    print(castingValue)
                    completion(castingValue,code)
                }
            case .failure(let error):
                let json = [String:Any]()
                completion(json,code)
            }
        }
    }
}





