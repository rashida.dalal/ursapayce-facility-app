//
//  Constants.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import Foundation
import Alamofire


class Constants {
    
#if DEBUG
    static var baseUrl: String = "https://stagedashboard.urspayce.com/"
    static var QRbaseUrl: String = "https://stageqr.urspayce.com/"
    static var displayImgPath = "https://s3.us-east-2.amazonaws.com/booking.urspayce.dev"
    
    
#else
    static var baseUrl: String = "https://dashboard.urspayce.com/"
    static var QRbaseUrl: String = "https://qr.urspayce.com/"
    static var displayImgPath = "https://s3.us-east-2.amazonaws.com/booking.urspayce.prod"
    
#endif

    static let PROGRESS_INDICATOR_VIEW_TAG:Int = 10
    
    static let version = "v1"
    
    static let ZULUTIME = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let NODATEZULUTIME = "HH:mm:ss.SSS'Z'"
    
    static let upload = baseUrl + version + "/marketing/bucket/upload"
    static let loginAuthenticate = baseUrl + version + "/user/users/authenticate"
    static let directoryList = baseUrl + version + "/user/users"
    static let companyList = baseUrl + version + "/user/company"
    
  //  static let diretoryApi = baseUrl + version + "/user/directory"
    
    
    static let generate_token = baseUrl + version + "/visitor/device/generate/token"
    static let verify_token = baseUrl + version + "/visitor/device/verify/token"
    static let task_schedule = baseUrl + version + "/task/task-schedule"
    static let checkCount = baseUrl + version + "/task/task-schedule/count"
    
    static let deleteDevice = baseUrl + version + "/visitor/device"
    static let add_review = baseUrl + version + "/add-review"
    
}

class APIManager {

    class func headers() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]

        if let token = userDefl.value(forKey: "Auth_token") as? String{
            headers["Authorization"] = "Bearer \(token)"
        }
        
        return headers
    }
}
