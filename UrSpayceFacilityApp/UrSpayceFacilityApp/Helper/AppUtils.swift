//
//  AppUtils.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import Foundation

import UIKit
import Reachability


extension UIViewController {
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func setShadowOnView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = 4
    }
    
    func setLightShadowOnView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 4
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
    }
    
    func multipleSetShadowOnView(views: [UIView]) {
        views.forEach { view in
            view.layer.masksToBounds = false
            view.layer.shadowColor = UIColor.lightGray.cgColor
            view.layer.shadowOffset = CGSize(width: 3, height: 2)
            view.layer.shadowOpacity = 0.5
            view.layer.shadowRadius = 5
        }
    }
    
    func multipleSetCornerRadius(views: [UIView]) {
        views.forEach { view in
            view.layer.cornerRadius = 8
        }
    }

    
    func setLightShadowOnViewWithCR(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 4
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 12.0
    }
    
    func convertTimeIntervalToLocalDateString(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:sssZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: date) ?? Date()

        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date )
        return timeStamp
    }
    
    func setTextFieldBorder(text: UITextField) {
        text.layer.borderWidth = 1
        text.layer.borderColor = UIColor.LighterGray.cgColor
        text.layer.cornerRadius = 5
        text.setLeftPaddingPoints(16)
    }
    
    func setMultipleTextFieldBorder(texts: [UITextField]) {
        texts.forEach { text in
            text.layer.borderWidth = 1
            text.layer.borderColor = UIColor.LighterGray.withAlphaComponent(0.8).cgColor
            text.layer.cornerRadius = 5
            text.setLeftPaddingPoints(16)
        }
    }
    
    func setLightShadowBorderOnView(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 4
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        view.layer.borderColor = UIColor.LighterGray.cgColor
    }

    
    func makeBackgroundBlur(_ view: UIView){
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        view.insertSubview(blurEffectView, at: 0)

    }
    
    
    
    func setLightShadowOnViewwithBorder(view: UIView) {
           view.layer.masksToBounds = false
        view.layer.borderColor = UIColor.BlueColor.cgColor
        view.layer.borderWidth = 1.0
           view.layer.shadowColor = UIColor.BlueColor.cgColor
           view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.4
           view.layer.shadowRadius = 4
        view.layer.cornerRadius = 8.0
       }
    
    func removeBorderandShadow(view: UIView) {
           view.layer.masksToBounds = false
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.borderWidth = 0
           view.layer.shadowColor = UIColor.clear.cgColor
           view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.4
           view.layer.shadowRadius = 4
       }
    
    func getIpAddress() -> String? {
            var address : String?
            
            // Get list of all interfaces on the local machine:
            var ifaddr : UnsafeMutablePointer<ifaddrs>?
            guard getifaddrs(&ifaddr) == 0 else { return nil }
            guard let firstAddr = ifaddr else { return nil }
            
            // For each interface ...
            for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
                let interface = ifptr.pointee
                
                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    // Check interface name:
                    let name = String(cString: interface.ifa_name)
                    if  name == "en0" {
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    } else if (name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3") {
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(1), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
            
            return address
        }

}

extension UITextView {
    
    func addDoneButton(title: String, target: Any, selector: Selector) {
        
        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: 0.0,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 44.0))//1
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
        toolBar.setItems([flexible, barButton], animated: false)//4
        self.inputAccessoryView = toolBar//5
    }
    
    

    
}

enum AssetsColor {
   case BlueButton
   case GreyButton
    case BorderView
    case Shadow
}


extension UIColor {

    static func appColor(_ name: AssetsColor) -> UIColor? {
        switch name {
        case .BlueButton:
            return UIColor(named: "buttonBG")
        case .GreyButton:
            return UIColor(named: "buttonGrayColor")
        case .BorderView:
            return UIColor(named: "borderColor")
        case .Shadow:
            return UIColor(named: "shadowColor")
        }
    }
}


extension String {
    var stringWidth: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.width
    }

}

extension UIViewController {
    
    
    
    func convertDateFormatter(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
    func convertTimeFormatter(date: String) -> String {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let timeSlotDate = dateFormatter.date(from: date)
        let newDate = DateFormatter()
        newDate.dateFormat = "hh:mm a"
        
        let result = newDate.string(from: timeSlotDate!)
        print(result)
        return result

        
    }
    
    func changeDateFormat(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "MMM dd yyyy"
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
    func convertDateWithouYEar(date: String, dateFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            return ""
        }
        dateFormatter.dateFormat = dateFormat
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
    func convertStringToDate(date: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "UTC")
        let convertedDate = dateFormatter.date(from: date)
        
        return convertedDate ?? Date()
    }
    
    func changeDateFormatForMonth(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            return ""
        }
        dateFormatter.dateFormat = "MMM"
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
    func changeDateFormatForDate(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            //assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "dd"
        let timeStamp = dateFormatter.string(from: convertedDate!)
        print(timeStamp)
        return timeStamp
    }
    
    
    

    func utcTolocal(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as? TimeZone
        let dateObj = dateFormatter.date(from: dateString)
        if let date = dateObj {
            return date
        } else {
            return Date()
        }
    }
    
    
    func convertTimeIntoHours(date: Date) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh" // output format
            let string = dateFormatter.string(from: date)
            return string
        }
        
    func get24HourFromTime(dateAsString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if dateAsString.contains("AM") || dateAsString.contains("PM") {
            dateFormatter.dateFormat = "hh:mm a"
        } else {
            dateFormatter.dateFormat = "hh:mm"
        }
        
        let date = dateFormatter.date(from: dateAsString) //returns nil
        let dateFormatter_1 = DateFormatter()
        dateFormatter_1.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter_1.dateFormat = "HH:mm"
        let strD = dateFormatter_1.string(from: date ?? Date())
        return strD
    }
}

extension UIView {
    func setShadowOnView(view: UIView) {
           view.layer.masksToBounds = false
           view.layer.shadowColor = UIColor.darkGray.cgColor
           view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.8
           view.layer.shadowRadius = 4
       }
    
    func setLightShadowOnView(view: UIView) {
           view.layer.masksToBounds = false
           view.layer.shadowColor = UIColor.LighterGray.cgColor
           view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowOpacity = 0.4
           view.layer.shadowRadius = 4
       }
    
    
}


extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}


extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}


extension Date {
    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: self, relativeTo: Date())
    }
}


extension Date {
    
    func getElapsedInterval(date: Date) -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: date)
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year" :
                "\(year)" + " " + "years"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month" :
                "\(month)" + " " + "months"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day" :
                "\(day)" + " " + "days"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour" :
                "\(hour)" + " " + "hours"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute" :
                "\(minute)" + " " + "minutes"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second" :
                "\(second)" + " " + "seconds"
        } else {
            return "a moment ago"
        }
        
    }
}

extension Date {
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    
    var nextHour: Date {
        return Calendar.current.date(byAdding: .hour, value: 6, to: self)!
    }
    
    var bufferMin: Date {
        return Calendar.current.date(byAdding: .minute, value: 5, to: self)!
    }
    
    var fifteenMinBefore: Date {
        return Calendar.current.date(byAdding: .minute, value: -15, to: self)!
    }
    
    var fifteenMinAfter: Date {
        return Calendar.current.date(byAdding: .minute, value: 15, to: self)!
    }

}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0,
                                               width: amount,
                                               height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0,
                                               width: amount,
                                               height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}




class ConnectionManager {

    static let shared = ConnectionManager()
    private init () {}

    func hasConnectivity() -> Bool {
        do {
            let reachability: Reachability = try Reachability()
            let networkStatus = reachability.connection
            
            switch networkStatus {
            case .unavailable:
                return false
            case .wifi, .cellular:
                return true
            default:
                print("internet")
                return true
            }
            
        }
        catch {
            return false
        }
    }
}

class AppUtility: NSObject
{
    public static func isValidEmail(emailString : String) -> Bool
    {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailTest.evaluate(with: emailString)
        {
            return true
        }
        return false
    }
    
    public static func getFormattedDate(fromFormate : String, toFormate : String, dateStr : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormate
        
        let dateVal = dateFormatter.date(from: dateStr)
        
        dateFormatter.dateFormat = toFormate
        
        return "\(dateFormatter.string(from: dateVal!))"
    }
    
    public static func isValidPassword(passwordString : String) -> Bool
    {
        let passwordRegEx = "(?=.*[A-Z])(?=.*[!@#$&*<>])(?=.*[0-9])(?=.*[a-z]).{8,}"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        
        if passwordTest.evaluate(with: passwordString)
        {
            return true
        }
        
        return false
    }
    
    public static func resizeImage(imageObj : UIImage) -> UIImage
    {
        var w : Float = 00.0000000000
        var h : Float = 00.0000000000
        
        w = Float((imageObj.size.width / imageObj.size.height) * 420)
        h = Float((imageObj.size.height / imageObj.size.width) * 380)
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(w), height: CGFloat(h)), false, 0.0)
        imageObj.draw(in: CGRect(x: 0.0, y: 0.0, width: CGFloat(w), height: CGFloat(h)))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public static func getUserData(keyVal : String) -> String
    {
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: keyVal as String) == nil
        {
            return ""
        }
        else
        {
            //            let valueString : String = preferences.value(forKey: keyVal as String) as! String
            
            //            let valueString : String = "\(preferences.value(forKey: keyVal as String) ?? "")"
            
            let valueString = preferences.value(forKey: keyVal)
            
            return "\(valueString ?? "")"
        }
    }
    
    public static func setUserData(keyString : String, valueString : String)
    {
        UserDefaults.standard.set("\(valueString)", forKey: "\(keyString)")
        UserDefaults.standard.synchronize()
    }
    
    public static func deleteAllUserDefaults()
    {
        for currentObj in UserDefaults.standard.dictionaryRepresentation().keys
        {
            if !(currentObj == "deviceToken")
            {
                UserDefaults.standard.removeObject(forKey: currentObj)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    public static func deleteAllKeysFromUserDef(){
        for currentObj in UserDefaults.standard.dictionaryRepresentation().keys
        {
            UserDefaults.standard.removeObject(forKey: currentObj)
            UserDefaults.standard.synchronize()
        }
    }
    
    public static func getStringFromJsonArray(data : NSMutableArray) -> String
    {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            if jsonData.count == 0 {
                return ""
            }
            else
            {
                let jsonString : String = (String(data: jsonData, encoding: .utf8)!).replacingOccurrences(of: "\\/", with: "/")
                return String(format: "%@",jsonString) as String
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
        return ""
    }
    
    public static func getBase64String(dataStr : String) -> String
    {
        let base64Str = dataStr.data(using: .utf8)?.base64EncodedString()
        
        return "\(base64Str ?? "")"
    }
    
    public static func fromBase64String(strVal : String) -> String
    {
        let decodedData = Data(base64Encoded: strVal)!
        let decodedString = String(data: decodedData, encoding: .utf8)!
        
        return decodedString
    }
    
    public static func convertToDictionary(text: String) -> [String: Any]?
    {
        if let data = text.data(using: .utf8)
        {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    public static func getHexString(hexString : String) -> String
    {
        var redColor : CGFloat = 0.0
        var greenColor : CGFloat = 0.0
        var blueColor : CGFloat = 0.0
        
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#"))
        {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6)
        {
            redColor = 232.0
            greenColor = 232.0
            blueColor = 232.0
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        redColor = CGFloat((rgbValue & 0xFF0000) >> 16)
        greenColor = CGFloat((rgbValue & 0x00FF00) >> 8)
        blueColor = CGFloat(rgbValue & 0x0000FF)
        
        if ((redColor * 0.299) + (greenColor * 0.587) + (blueColor * 0.114)) > 186
        {
            return "#000000"
        }
        else
        {
            return "#FFFFFF"
        }
        
    }
}




public extension UIViewController{
    class func getReferenceFromStoryBoard(_ storyBoard: String) -> Self{
        return instantiateFromStoryBoardHelper(storyBoard)
    }
    
    fileprivate class func instantiateFromStoryBoardHelper<T>(_ name:String) -> T{
        let storyBoard = UIStoryboard(name: name, bundle: nil)
        let identifier = String.init(describing: self)
        return storyBoard.instantiateViewController(withIdentifier: identifier) as! T
    }
}


@IBDesignable open class RCustomButton: UIButton {
    
    /// When positive, the background of the layer will be drawn with rounded corners. Also effects the mask generated by the `masksToBounds' property. Defaults to zero. Animatable.
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    /// The width of the layer's border, inset from the layer bounds. The border is composited above the layer's content and sublayers and includes the effects of the `cornerRadius' property. Defaults to zero. Animatable.
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    /// The color of the layer's border. Defaults to opaque black. Colors created from tiled patterns are supported. Animatable.
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    
    
}
extension CALayer {
    func applyCornerRadiusShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0,
        cornerRadiusValue: CGFloat = 0)
    {
        cornerRadius = cornerRadiusValue
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
