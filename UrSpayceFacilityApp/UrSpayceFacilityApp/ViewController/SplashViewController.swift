//
//  SplashViewController.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var lblTitle : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.lblTitle.text = "Welcome to"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.moveToNext()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func moveToNext() {
        
        if let session = userDefl.value(forKey: "isLogin") as? Bool{
            if session{
                let directory = TaskListVC.getReferenceFromStoryBoard("Main")
                self.navigationController?.pushViewController(directory, animated: true)
            }else{
                userDefl.removeObject(forKey: "token_connect")
                let device = DeviceViewController.getReferenceFromStoryBoard("Main")
                self.navigationController?.pushViewController(device, animated: true)
            }
        }else{
            userDefl.removeObject(forKey: "token_connect")
            let device = DeviceViewController.getReferenceFromStoryBoard("Main")
            self.navigationController?.pushViewController(device, animated: true)
        }

    }
}
