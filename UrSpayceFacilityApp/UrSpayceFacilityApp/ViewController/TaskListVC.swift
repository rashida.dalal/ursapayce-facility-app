//
//  TaskListVC.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import UIKit
import SDWebImage

var logoutTimer : Timer?
var isCameraBack = false

class TaskListVC: UIViewController {
    
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var lblTime : UILabel!
    
    @IBOutlet weak var tblTaskList : UITableView!
    
    @IBOutlet weak var lblTotalCount : UILabel!
    @IBOutlet weak var lblPending : UILabel!
    
    @IBOutlet weak var authenticateView : UIStackView!
    @IBOutlet weak var CheckInOutView : UIView!
    
    @IBOutlet weak var hgtBottomView : NSLayoutConstraint!
    
    @IBOutlet weak var loaderView: UIActivityIndicatorView!
    
    @IBOutlet weak var ApiLoaderView: UIActivityIndicatorView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var viewReview: UIView!
    
    @IBOutlet weak var viewCIn: UIView!
    @IBOutlet weak var viewCOut: UIView!
    
    var arrTaskList = [TaskListData]()
    var selectedTask = [TaskListData]()
    
    
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    var qrDataBrand: QRData? = nil {
        didSet {
            if qrDataBrand != nil {
                self.callApiAfterScan(strId: qrDataBrand?.codeString ?? "")
            }
        }
    }
    
    var employeeId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let strtTime = formatter.string(from: Date())
        self.lblTime.text = strtTime
        self.scannerView.layer.cornerRadius = 8.0
        
        logoutTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { _ in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let strtTime = formatter.string(from: Date())
            self.lblTime.text = strtTime
            self.verifyDevice()
        })
        
        
        var brandId = ""
        if let bId = userDefl.value(forKey: "brandId") as? String{
            brandId = bId
        }
        let imgNAme = userDefl.value(forKey: "brandImage") as! String
        
        if imgNAme == "" {
            self.imgLogo.isHidden = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            tap.numberOfTapsRequired = 3
            self.view.isUserInteractionEnabled = true
            self.view.addGestureRecognizer(tap)
        }else{
            self.imgLogo.isHidden = false
            let ImgUrl = Constants.displayImgPath + "/\(brandId)" + "/venue/images/\(imgNAme)"
            print(ImgUrl)
            
            self.imgLogo.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imgLogo.sd_setImage(with: URL(string: ImgUrl),
                                     placeholderImage: UIImage(named: ""))
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            tap.numberOfTapsRequired = 3
            self.imgLogo.isUserInteractionEnabled = true
            self.imgLogo.addGestureRecognizer(tap)
        }
        
        
        self.ApiLoaderView.isHidden = false
        self.viewBottom.isHidden = true
        
        
        self.tblTaskList.register(UINib(nibName: "TblTaskCell", bundle: nil), forCellReuseIdentifier: "TblTaskCell")
        self.tblTaskList.estimatedRowHeight = UITableView.automaticDimension
        self.tblTaskList.rowHeight = UITableView.automaticDimension
        
        self.callTaskList()
        

        
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.logOut()
    }
    
    func logOut(){
        if let device_id = userDefl.value(forKey: "device_id") as? String {
            let url = "\(Constants.deleteDevice)/\(device_id)"
            print(url)
            APIClient.deleteDevice(delUrl: url) { resp, status in
                logoutTimer?.invalidate()
                logoutTimer = nil
                self.removeData()
            }
        }
    }
    
    func callTaskList(){
        
        var brandId = ""
        if let bId = userDefl.value(forKey: "brandId") as? String{
            brandId = bId
        }
        let venueId =  userDefl.value(forKey: "venue_id") as! String
        let param = ["brand":brandId,
                    "venue":venueId] as! [String : Any]
        
        APIClient.getTaskList(param: param) { response, status in
            
            if status == 200
            {
                let count = response.count ?? 0
                self.lblTotalCount.text = "\(count)"
                self.lblPending.text = "0"
                
                let data = response.data ?? []
                if data.count > 0
                {
                    for obj in data
                    {
                        self.arrTaskList.append(obj)
                    }
                    
                    self.tblTaskList.dataSource = self
                    self.tblTaskList.delegate = self
                    self.tblTaskList.reloadData()
                    
                    
                    self.ApiLoaderView.isHidden = true
                    self.viewBottom.isHidden = false

                    
                    self.CheckInOutView.isHidden = true
                    self.viewReview.isHidden = true
                    
                    self.authenticateView.isHidden = false
                    self.hgtBottomView.constant = 200
                    self.loaderView.isHidden = true

                    if !self.scannerView.isRunning {
                        self.scannerView.startScanning()
                    }


                }
                else{
                    
                    self.ApiLoaderView.isHidden = true
                    self.viewBottom.isHidden = false

                    
                    self.CheckInOutView.isHidden = true
                    self.viewReview.isHidden = true
                    self.authenticateView.isHidden = false
                    self.hgtBottomView.constant = 200
                    self.loaderView.isHidden = true

                    if !self.scannerView.isRunning {
                        self.scannerView.startScanning()
                    }


                }
                
            }else{
                
                
            }
        }
    }
    
}


extension TaskListVC {
    
    func verifyDevice(){
        
        if let token_connect = userDefl.value(forKey: "token_connect") as? String {
            
            let param = ["token": token_connect,
                         "app":"Facility"] as [String: Any]
            APIClient.verifyToken(param: param) { resp, statusCode in
                
                if statusCode == 200
                {
                    let verify = resp.verified ?? false
                    if verify {
                        
                        OperationQueue.main.addOperation {
                            
                            let creator = resp.creator ?? ""
                            userDefl.set(creator, forKey: "user_id")
                            
                            let venue = resp.venue
                            let venueId = venue?._id ?? ""
                            
                            let busiPArkId = venue?.buildingType ?? ""
                            
                            let brand = resp.brand
                            let brandId = brand?._id ?? ""
                            let brandName = brand?.name ?? ""
                            let imgNAme = brand?.image?.name ?? ""
                            
                            let country = venue?.location?.country ?? "India"
                            
                            // let dialCode = brand?.phone?.dialCode ?? ""
                            
                            let deviceC = resp.device?._id ?? ""
                            
                            let token = resp.authToken ?? ""
                            
                            
                            
                            userDefl.set(true, forKey: "isLogin")
                            userDefl.set(token, forKey: "Auth_token")
                            userDefl.set(venueId, forKey: "venue_id")
                            userDefl.set(brandId, forKey: "brandId")
                            userDefl.set(brandName, forKey: "brandName")
                            userDefl.set(imgNAme, forKey: "brandImage")
                            userDefl.set(country, forKey: "dialCode")
                            userDefl.set(busiPArkId, forKey: "business_park_id")
                            userDefl.set(deviceC, forKey: "device_id")
                            
                            userDefl.synchronize()
                            
                            
                            
                            
                            
                            
                        }
                    }
                    else {
                        logoutTimer?.invalidate()
                        logoutTimer = nil
                        self.removeData()
                    }
                }
            }
        }
    }
}


extension TaskListVC {
    
    @IBAction func clickOncheckInBtn(_ sender : UIButton)
    {
        
        for obj in self.arrTaskList
        {
            let selct = obj.select
            if selct {
                self.selectedTask.append(obj)
            }
        }
        
        if self.selectedTask.count == 0
        {
            let alert = UIAlertController(title: "", message: "Please select task for Check In.", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        else{
            
            for i in 0..<self.selectedTask.count
            {
                let obj = self.selectedTask[i]
                let taskId = obj._id ?? ""
                var brandId = ""
                if let bId = userDefl.value(forKey: "brandId") as? String{
                    brandId = bId
                }
                let venueId =  userDefl.value(forKey: "venue_id") as! String
                let param = ["brand":brandId,
                            "venue":venueId,
                             "status": "InProgress",
                             "assignedTo":self.employeeId,
                             "_id":taskId] as! [String : Any]

                APIClient.updateTaskList(param: param) { resp, status in
                    
                    if status == 200
                    {
                        if i == self.selectedTask.count - 1 {
                            //lastElement contains the last element
                            let schedule = AlertSucessVC.getReferenceFromStoryBoard("Main")
                            schedule.modalPresentationStyle = .overCurrentContext
                            schedule.pageOpenFrom = "checkIn"
                            schedule.taskCount = self.selectedTask.count
                            schedule.comPCalback = { [weak self] stats in
                                    guard let sSelf = self else {return}
                                
                                let param = ["brand":brandId,
                                             "status": "InProgress",
                                             "assignedTo":sSelf.employeeId,
                                            "venue":venueId] as! [String : Any]
                                
                                APIClient.getTaskList(param: param) { response, status in
                                    
                                    if status == 200
                                    {
                                        let count = response.count ?? 0
                                        sSelf.lblTotalCount.text = "\(count)"
                                        sSelf.lblPending.text = "0"
                                        
                                        sSelf.arrTaskList.removeAll()
                                        
                                        let data = response.data ?? []
                                        if data.count > 0
                                        {
                                            for obj in data
                                            {
                                                sSelf.arrTaskList.append(obj)
                                            }
                                            
                                            
                                            for i in 0..<sSelf.arrTaskList.count
                                            {
                                                var obj = sSelf.arrTaskList[i]
                                                obj.showSelectView = true
                                                obj.select = false
                                                sSelf.arrTaskList.remove(at: i)
                                                sSelf.arrTaskList.insert(obj, at: i)
                                            }
                                            
                                            sSelf.selectedTask.removeAll()
                                            
                                            sSelf.viewCIn.isHidden = true
                                            sSelf.viewCOut.isHidden = false
                                            
                                            sSelf.tblTaskList.dataSource = self
                                            sSelf.tblTaskList.delegate = self
                                            sSelf.tblTaskList.reloadData()

                                        }
                                    }
                                }
                            }
                            self.present(schedule, animated: true) {
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func clickOncheckOutBtn(_ sender : UIButton)
    {
        
        for obj in self.arrTaskList
        {
            let selct = obj.select
            if selct {
                self.selectedTask.append(obj)
            }
        }
        
        if self.selectedTask.count == 0
        {
            let alert = UIAlertController(title: "", message: "Please select task for Check Out.", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        else{
            
            for i in 0..<self.selectedTask.count
            {
                let obj = self.selectedTask[i]
                let taskId = obj._id ?? ""
                var brandId = ""
                if let bId = userDefl.value(forKey: "brandId") as? String{
                    brandId = bId
                }
                let venueId =  userDefl.value(forKey: "venue_id") as! String
                let param = ["brand":brandId,
                            "venue":venueId,
                             "status": "Resolved",
                             "assignedTo":self.employeeId,
                             "_id":taskId] as! [String : Any]

                APIClient.updateTaskList(param: param) { resp, status in
                    
                    if status == 200
                    {
                        if i == self.selectedTask.count - 1 {
                            //lastElement contains the last element
                            let schedule = AlertSucessVC.getReferenceFromStoryBoard("Main")
                            schedule.modalPresentationStyle = .overCurrentContext
                            schedule.pageOpenFrom = "checkOut"
                            schedule.taskCount = self.selectedTask.count
                            schedule.comPCalback = { [weak self] stats in
                                    guard let sSelf = self else {return}
                                
                                let param = ["brand":brandId,
                                            "venue":venueId] as! [String : Any]
                                
                                APIClient.getTaskList(param: param) { response, status in
                                    
                                    if status == 200
                                    {
                                        let count = response.count ?? 0
                                        sSelf.lblTotalCount.text = "\(count)"
                                        sSelf.lblPending.text = "0"
                                        
                                        sSelf.arrTaskList.removeAll()
                                        sSelf.selectedTask.removeAll()
                                        
                                        let data = response.data ?? []
                                        if data.count > 0
                                        {
                                            for obj in data
                                            {
                                                sSelf.arrTaskList.append(obj)
                                            }
                                            
                                            
                                            for i in 0..<sSelf.arrTaskList.count
                                            {
                                                var obj = sSelf.arrTaskList[i]
                                                obj.showSelectView = true
                                                obj.select = false
                                                sSelf.arrTaskList.remove(at: i)
                                                sSelf.arrTaskList.insert(obj, at: i)
                                            }
                                            
                                            sSelf.viewCIn.isHidden = false
                                            sSelf.viewCOut.isHidden = true
                                            
                                            sSelf.tblTaskList.dataSource = self
                                            sSelf.tblTaskList.delegate = self
                                            sSelf.tblTaskList.reloadData()

                                        }
                                    }
                                }
                            }
                            self.present(schedule, animated: true) {
                            }

                        }
                    }
                }
            }
        }
    }
    
    @IBAction func clickOnReviewBtn(_ sender : UIButton)
    {
        
        var arrayReview = [TaskListData]()
        for obj in self.arrTaskList
        {
            let status = obj.status
            let selct = obj.select
            if selct {
                if status == "Resolved"
                {
                    arrayReview.append(obj)
                }
            }
        }
        
        if arrayReview.count == 0
        {
            let alert = UIAlertController(title: "", message: "Please select Resolved task for Review.", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        else{
            
            let schedule = ReviewViewController.getReferenceFromStoryBoard("Main")
            schedule.modalPresentationStyle = .overCurrentContext
            schedule.passReviewArray = arrayReview
            schedule.comPCalback = { [weak self] stats in
                    guard let sSelf = self else {return}
                
                arrayReview.removeAll()
                
                for i in 0..<sSelf.arrTaskList.count
                {
                    var obj = sSelf.arrTaskList[i]
                    obj.select = false
                    sSelf.arrTaskList.remove(at: i)
                    sSelf.arrTaskList.insert(obj, at: i)

                }
                sSelf.tblTaskList.reloadData()
            }
            self.present(schedule, animated: true) {
            }

        }

    }
}


extension TaskListVC: UITableViewDataSource, UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTaskList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TblTaskCell",
                                                 for: indexPath) as! TblTaskCell
        
        let obj = self.arrTaskList[indexPath.row]
        let showSelectView = obj.showSelectView
        if showSelectView
        {
            cell.viewChekBoxes.isHidden = false
                let select = obj.select
                if select {
                    cell.imgTick.image = UIImage(named: "check")
                }else{
                    cell.imgTick.image = UIImage(named: "uncheck")
                }
        }
        else{
            cell.viewChekBoxes.isHidden = true
        }
        
        let title = obj.title ?? ""
        cell.lblTask.text = title
        
        let status = obj.status ?? ""
        cell.lblStatus.text = status

        let floor = obj.floor ?? ""
        cell.lblLoc.text = floor
        
        let starDat = obj.section?.startDate ?? ""
        let df = DateFormatter()
        df.dateFormat = Constants.ZULUTIME
        let startDt = df.date(from:starDat)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let showTime = dateFormatter.string(from: startDt)
        cell.lblTat.text = showTime

        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var obj = self.arrTaskList[indexPath.row]
        let taskId = obj._id ?? ""
        let showSelectView = obj.showSelectView
        if showSelectView
        {
            let select = obj.select
            if select {
                
                obj.select = false
                self.arrTaskList.remove(at: indexPath.row)
                self.arrTaskList.insert(obj, at: indexPath.row)
                self.tblTaskList.reloadData()

            }
            else{
                
                obj.select = true
                self.arrTaskList.remove(at: indexPath.row)
                self.arrTaskList.insert(obj, at: indexPath.row)
                self.tblTaskList.reloadData()

            }
        }



    }
    
}


extension TaskListVC: QRScannerViewDelegate {
    
    func qrScanningDidStop(){}
    
    func qrScanningDidFail() {
        print("Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrDataBrand = QRData(codeString: str)
    }
    
    func callApiAfterScan(strId : String){
        
        
            
            print(strId)
            
            if strId.contains("employee-pass") {
                
                let url = strId.components(separatedBy: "/")
                let empId = url.last
                self.loaderView.isHidden = false
                self.loaderView.startAnimating()
                self.employeeId = empId ?? ""
                self.getCheckListCount()
            }
            else{
                
                let refreshAlert = UIAlertController(title: "", message: "Pass not available.", preferredStyle: UIAlertController.Style.alert)
                self.present(refreshAlert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3){
                    refreshAlert.dismiss(animated: true, completion: nil)
                }
                
                if !self.scannerView.isRunning {
                    self.scannerView.startScanning()
                }
                
                self.loaderView.isHidden = true
                self.loaderView.stopAnimating()
                
            }
        
    }
    
    
    func getCheckListCount(){
        
        var brandId = ""
        if let bId = userDefl.value(forKey: "brandId") as? String{
            brandId = bId
        }
        let venueId =  userDefl.value(forKey: "venue_id") as! String
        let param = ["assignedTo":self.employeeId,
                     "status": "InProgress",
                      "brand":brandId,
                      "venue":venueId]
        print(param)
        
        APIClient.getCheckCount(param: param) { resp, status in
            
            if status == 200
            {
                print(resp)
                
                if let count = resp as? Int
                {
                    self.loaderView.isHidden = true
                    if self.scannerView.isRunning {
                        self.scannerView.stopScanning()
                    }
                    self.hgtBottomView.constant = 150
                    self.authenticateView.isHidden = true
                    self.CheckInOutView.isHidden = false
                    self.viewReview.isHidden = false

                   if count == 0
                   {
                       self.viewCIn.isHidden = false
                       self.viewCOut.isHidden = true
                       
                       for i in 0..<self.arrTaskList.count
                       {
                           var obj = self.arrTaskList[i]
                           obj.showSelectView = true
                           self.arrTaskList.remove(at: i)
                           self.arrTaskList.insert(obj, at: i)
                       }
                       self.tblTaskList.reloadData()
                   }
                    else {
                        
                        self.viewCIn.isHidden = true
                        self.viewCOut.isHidden = false
                        
                        let param = ["brand":brandId,
                                     "status": "InProgress",
                                     "assignedTo":self.employeeId,
                                    "venue":venueId] as! [String : Any]
                        
                        APIClient.getTaskList(param: param) { response, status in
                            
                            if status == 200
                            {
                                let count = response.count ?? 0
                                self.lblTotalCount.text = "\(count)"
                                self.lblPending.text = "0"
                                
                                
                                self.arrTaskList.removeAll()
                                
                                let data = response.data ?? []
                                if data.count > 0
                                {
                                    for obj in data
                                    {
                                        self.arrTaskList.append(obj)
                                    }
                                    
                                    for i in 0..<self.arrTaskList.count
                                    {
                                        var obj = self.arrTaskList[i]
                                        obj.showSelectView = true
                                        self.arrTaskList.remove(at: i)
                                        self.arrTaskList.insert(obj, at: i)
                                    }
                                    
                                    
                                    self.tblTaskList.dataSource = self
                                    self.tblTaskList.delegate = self
                                    self.tblTaskList.reloadData()

                                }
                                
                            }
                        }

                    }
                }
            }
        }
    }
    
    
    @IBAction func clickOnRotate(_ sender: UIButton){
        
        if sender.isSelected {
            sender.isSelected = false
            isCameraBack = false
        } else {
            sender.isSelected = true
            isCameraBack = true
        }
        self.scannerView.reStartCam()

    }

}


extension TaskListVC {
    
    func removeData(){
        
        userDefl.removeObject(forKey: "token_connect")
        userDefl.removeObject(forKey: "isLogin")
        userDefl.removeObject(forKey: "Auth_token")
        userDefl.removeObject(forKey: "venue_id")
        userDefl.removeObject(forKey: "brandId")
        userDefl.removeObject(forKey: "brandName")
        userDefl.removeObject(forKey: "brandImage")
        userDefl.removeObject(forKey: "dialCode")
        userDefl.removeObject(forKey: "business_park_id")
        userDefl.removeObject(forKey: "device_id")
        userDefl.removeObject(forKey: "user_id")
        
        let notice = SplashViewController.getReferenceFromStoryBoard("Main")
        self.navigationController?.pushViewController(notice, animated: false)

    }
}
