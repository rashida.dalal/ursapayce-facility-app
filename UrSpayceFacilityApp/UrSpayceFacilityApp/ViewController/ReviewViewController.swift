//
//  ReviewViewController.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 23/01/24.
//

import UIKit

class ReviewViewController: UIViewController {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet var viewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var starRatingView: SwiftyStarRatingView!
    @IBOutlet weak var viewReason: UIView!
    @IBOutlet weak var txtReason: UITextView!
    
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    
    @IBOutlet weak var loaderView: UIActivityIndicatorView!
    
    var placeholderLabel : UILabel!
    
    var givenRateVal = ""
    var passReviewArray = [TaskListData]()

    typealias popUpCalback = (_ status : String) ->()
    var comPCalback:popUpCalback?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loaderView.isHidden = true
        
        self.viewMain.layer.borderWidth = 1.0
        self.viewMain.layer.borderColor = UIColor.borderColor.cgColor
        self.viewMain.layer.applyCornerRadiusShadow(color: .lightGray,
                                                      alpha: 50,
                                                      x: 0, y: 4,
                                                      blur: 30,
                                                      spread: 0,
                                                      cornerRadiusValue: 20)
        self.viewReason.layer.borderWidth = 1.0
        self.viewReason.layer.borderColor = UIColor.borderColor.cgColor
        self.viewReason.layer.cornerRadius = 12.0
        
        txtReason.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter reason here..."
        placeholderLabel.font = .italicSystemFont(ofSize: (txtReason.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        txtReason.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtReason.font?.pointSize)! / 2)
        placeholderLabel.textColor = .tertiaryLabel
        placeholderLabel.isHidden = !txtReason.text.isEmpty
        
        signatureView.delegate = self
        
    }
}

extension ReviewViewController {
    
    @IBAction func starRatingValueChanged(_ sender: SwiftyStarRatingView) {
        starRatingViewValueChange()
    }
    
    func starRatingViewValueChange() {
        givenRateVal = "\(starRatingView.value)"
        print(givenRateVal)
    }
    

    
    @IBAction func clickOmBack(_ sender: UIButton){
        guard let cb = self.comPCalback else {return}
        cb("true")
        self.dismiss(animated: true)
    }
    
    @IBAction func clickSubmitBtn(_ sender: UIButton){
        
        self.loaderView.isHidden = false
        for i in 0..<self.passReviewArray.count
        {
            let obj = self.passReviewArray[i]
            let taskId = obj._id ?? ""
            var brandId = ""
            if let bId = userDefl.value(forKey: "brandId") as? String{
                brandId = bId
            }
            let venueId =  userDefl.value(forKey: "venue_id") as! String
            let param = ["brand":brandId,
                        "venue":venueId,
                         "description":self.txtReason.text!,
                         "rating": self.givenRateVal] as! [String : Any]

                 APIClient.SubmitReview(taskId: taskId, param: param) { response, status in
                     
                     if i == self.passReviewArray.count - 1 {
                         guard let cb = self.comPCalback else {return}
                         cb("true")
                         self.dismiss(animated: true)

                     }

                 }
                 


        }
        

    }
    
}

extension ReviewViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel?.isHidden = !textView.text.isEmpty
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        placeholderLabel?.isHidden = !textView.text.isEmpty
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholderLabel?.isHidden = true
    }
}


//


extension  ReviewViewController: YPSignatureDelegate {
    
    func didStart(_ view : YPDrawSignatureView) {
    }
    
    func didFinish(_ view : YPDrawSignatureView) {
        

    }

    
    
}

