//
//  TblTaskCell.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import UIKit

class TblTaskCell: UITableViewCell {
    
    @IBOutlet weak var lblTask : UILabel!
    @IBOutlet weak var lblLoc : UILabel!
    @IBOutlet weak var lblTat : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var viewChekBoxes : UIView!
    
    @IBOutlet weak var imgTick : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
