//
//  DeviceViewController.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import UIKit
import FirebaseAnalytics

class DeviceViewController: UIViewController {
    
    @IBOutlet weak var labelToken : UILabel!
    
    var timer = Timer()
    @IBOutlet weak var loader : UIActivityIndicatorView!
    
    var TokenDevice = ""


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let ipName = getIpAddress() ?? ""
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        self.loader.isHidden = true
        let param = [
                    "name": UIDevice.current.name,
                    "deviceType":"ios",
                     "osVersion": UIDevice.current.systemVersion,
                     "ipAddress":ipName,
                     "visitorsVersion":bundleVersion ?? "1.4",
                    "app":"Facility"] as [String: Any]
        
        APIClient.getTokenToConnect(param: param) { response, statusCode in
            
            if statusCode == 200 {
                
                if let result = response["result"] as? [String:Any]{
                    if let token = result["token"] as? String{
                        self.TokenDevice = token
                        userDefl.set(self.TokenDevice, forKey: "token_connect")
                        self.labelToken.text = token.unfoldSubSequences(limitedTo: 3).joined(separator: " ")
                        userDefl.synchronize()
                    }
                }
            }
            else{
                APIClient.showAlertMessage(vc: self, titleStr: "Facility app", messageStr: "Api error.Status code --\(statusCode ?? 498)")
            }
            
        }
        
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { _ in
            self.verifyDevice()
        })

    }
}

extension DeviceViewController {
    
    func verifyDevice(){
        let param = ["token": self.TokenDevice,
                     "app":"Facility"] as [String: Any]
        APIClient.verifyToken(param: param) { resp, statusCode in
            
            if statusCode == 200
            {
                OperationQueue.main.addOperation {
                    
                    let verify = resp.verified ?? false
                    if verify {
                        
                        self.timer.invalidate()
                        
                        self.loader.isHidden = false
                        self.loader.startAnimating()
                        
                        let creator = resp.creator ?? ""
                        userDefl.set(creator, forKey: "user_id")

                        let venue = resp.venue
                        let venueId = venue?._id ?? ""
                        
                        let busiPArkId = venue?.buildingType ?? ""
                        
                        let brand = resp.brand
                        let brandId = brand?._id ?? ""
                        let brandName = brand?.name ?? ""
                        let imgNAme = brand?.image?.name ?? ""
                        
                        let country = venue?.location?.country ?? "India"
                        
                       // let dialCode = brand?.phone?.dialCode ?? ""
                        
                        let deviceC = resp.device?._id ?? ""
                        let token = resp.authToken ?? ""
                        
                        userDefl.set(true, forKey: "isLogin")
                        userDefl.set(token, forKey: "Auth_token")
                        userDefl.set(venueId, forKey: "venue_id")
                        userDefl.set(brandId, forKey: "brandId")
                        userDefl.set(brandName, forKey: "brandName")
                        userDefl.set(imgNAme, forKey: "brandImage")
                        userDefl.set(country, forKey: "dialCode")
                        userDefl.set(busiPArkId, forKey: "business_park_id")
                        userDefl.set(deviceC, forKey: "device_id")
                        userDefl.synchronize()
                        
                        self.loader.isHidden = true
                        self.loader.stopAnimating()
                        
                        Analytics.logEvent("Device_Pair_Event", parameters: [
                               "device_id" : deviceC,
                               "creator":creator
                        ])

                        let directory = TaskListVC.getReferenceFromStoryBoard("Main")
                        self.navigationController?.pushViewController(directory, animated: true)

                    }
                }
            }
        }
    }
}

extension Collection {

    func unfoldSubSequences(limitedTo maxLength: Int) -> UnfoldSequence<SubSequence,Index> {
        sequence(state: startIndex) { start in
            guard start < endIndex else { return nil }
            let end = index(start, offsetBy: maxLength, limitedBy: endIndex) ?? endIndex
            defer { start = end }
            return self[start..<end]
        }
    }

    func every(n: Int) -> UnfoldSequence<Element,Index> {
        sequence(state: startIndex) { index in
            guard index < endIndex else { return nil }
            defer { let _ = formIndex(&index, offsetBy: n, limitedBy: endIndex) }
            return self[index]
        }
    }

    var pairs: [SubSequence] { .init(unfoldSubSequences(limitedTo: 2)) }
}

extension StringProtocol where Self: RangeReplaceableCollection {

    mutating func insert<S: StringProtocol>(separator: S, every n: Int) {
        for index in indices.every(n: n).dropFirst().reversed() {
            insert(contentsOf: separator, at: index)
        }
    }

    func inserting<S: StringProtocol>(separator: S, every n: Int) -> Self {
        .init(unfoldSubSequences(limitedTo: n).joined(separator: separator))
    }
}
