//
//  AlertSucessVC.swift
//  UrSpayceFacilityApp
//
//  Created by Rashida on 10/01/24.
//

import UIKit

class AlertSucessVC: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var viewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTaskCount: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    var pageOpenFrom = ""
    typealias popUpCalback = (_ status : String) ->()
    var comPCalback:popUpCalback?
    var taskCount = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        let strtTime = formatter.string(from: Date())
        self.lblTime.text = strtTime

        
        self.viewMain.layer.borderWidth = 1.0
        self.viewMain.layer.borderColor = UIColor.borderColor.cgColor
        self.viewMain.layer.applyCornerRadiusShadow(color: .lightGray,
                                                      alpha: 50,
                                                      x: 0, y: 4,
                                                      blur: 30,
                                                      spread: 0,
                                                      cornerRadiusValue: 20)
        
        self.lblTaskCount.text = "\(taskCount) Task Submitted"
        
        if pageOpenFrom == "checkIn"
        {
            self.lblTitle.text = "Check In Successfully!"
        }else{
            self.lblTitle.text = "Check Out Successfully!"

        }
        
    }
    

    

}

extension AlertSucessVC {
    
    
    @IBAction func clickOmBack(_ sender: UIButton){
        guard let cb = self.comPCalback else {return}
        cb("true")
        self.dismiss(animated: true)
    }
    
}
